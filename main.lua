hero = {}

--images  

hero.immobile[1] = love.graphics.newImage ()

hero.move [1] = love.graphics.newImage ()

hero.attack [1] =      love.graphics.newImage ()

hero.x = nil
hero.y = nil
hero.jumpCond = false

-- enemy

enemy = {}	

-- lv1
  enemy.lv1 = {}
 -- move 
  enemy.lv1.move = love.graphics.newImage ()
  -- attack
  enemy.lv1.attack = love.graphics.newImage ()
  enemy.lv1.x = nil
  enemy.lv1.y = nil
  
-- tirs
  tir = {}
  tir.x = 0
  tir.y = 0
  tir.image = love.graphics.newImage ()
  
-- frame pour sprites
idAttack = 0
idMove = 0
  

  
function love.load ()

  -- hero 
  CreateSprite (hero.immobile, hero.x,  hero.y)

  -- ennemies
  CreateSprite (enemy.lv1.move [1], enemy.lv1.x, enemy.lv1.y)
  
  -- conditions pour : 
  hero.moveCond = false
  hero.immobileCond = false
  hero.attackCond = false
  
  -- tirs
  tir.vx = 10 -- A TESTER
  
  -- timer
  -- temps de décalage entre les attaques ennemy en fonction d'ID
  timerAttackEnemy = 0 
  timerAttackHero = 0
  
end 

function love.update (dt)

-- frame
	if timeMove == true then
    	idMove = idMove + 0.1 
	else
		idMove = 0
  end
    	
  if timeAttack == true then
		idAttack = idAttack + 0.1
	else
		idAttack = 0
	end
	
-- jump hero
  if hero.jumpCond == true then
  	timerJump = timerJump + 0.1
	end
		
  -- tirs
  tir.x = tir.x + tir.vx
  
  
  -- ennemie
  
  -- hero etage 1 
  if hero.x < 100 then
  
  end
  
end

function love.draw()

	if hero.immobileCond == true then
		CreateSprite(hero.immobile, hero.x, hero.y) 
	end
	
	if hero.moveCond == true then 
		CreateSprite(hero.move [idMove], hero.x, hero.y)
	end
	
	if hero.attackCond == true then 
		CreateSprite(hero.attack [idAttack], hero.x, hero.y)
	end

end

function love.keypressed (key)

  if key == q or key == left then
    hero.x = hero.x - 10
    hero.moveCond = true
    timeMove = true
  else
    hero.immobile = true
    timeMove = false
  end
  
  if key == d or key == right then
    hero.x = hero.x + hero.x
    hero.moveCond = true
    timeMove = true
  else
    hero.immobile = true
    timeMove = false
  end
  
  if hero.jumpCond == true then
  
	  if key == space or key == up then
     -- hero.y 
 	  else
      hero.jumpCond = false
      timeJump = 0
    end
 	 
  end
  
end
   
function love.mousepressed (button, x, y) 
  
  if button == 1 then
    CreateSprite (tir.image, tir.x, tir.y) 
    timeAttack = true	
  else 
    timeAttack = false
  end
   
end

function CreateSprite(pName,pX,pY)

  love.graphics.draw(pName, pX, pY)
  
end